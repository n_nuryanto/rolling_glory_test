import axios from 'axios'

export const state = () => ({
  
})

export const getters = {
  
}


export const mutations = {
 
}

export const actions = {
  async loadApiGetGeneral({ commit }, payload) {
    try {
      const response = await axios.get('/out/' + payload.endPoint)
      return Promise.resolve(response.data)
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async loadApiPostGeneral({ commit }, {endPoint, payload}) {
    try {
      const response = await axios.post(
        '/out/' + endPoint,
        payload
      )
      return Promise.resolve(response.data)
    } catch (error) {
      console.log(error.response)
      return Promise.resolve(error.response.data)
    }
  },
  
  
  
}

export const strict = false
